#ifndef M_PI
#define M_PI 3.141592653589793238
#endif

#pragma once
#include <math.h>
#include <iostream>
#include <fstream>
#include <string>

//****************************************************************************
// 2D double Vertex Class
//****************************************************************************
class Vertex2D{
public:
	// どっちでも定義できる
	union { struct { double x, y; }; double X[2]; };

	// 初期化
	Vertex2D() :x(0), y(0){}
	Vertex2D(double _x, double _y) :x(_x), y(_y){}
	Vertex2D(double _X[2]) :x(_X[0]), y(_X[1]){}

	Vertex2D &operator=(const Vertex2D &v){ this->x = v.x; this->y = v.y; return(*this); }
	Vertex2D &operator=(const double &v){ this->x = v; this->y = v; return(*this); }
	Vertex2D &operator+=(const Vertex2D &u){ x += u.x; y += u.y; return(*this); }
	Vertex2D &operator-=(const Vertex2D &u){ x -= u.x; y -= u.y; return(*this); }
	Vertex2D &operator*=(const Vertex2D &u){ x *= u.x; y *= u.y; return(*this); }
	Vertex2D &operator*=(const double a){ x *= a; y *= a; return(*this); }
	Vertex2D &operator/=(const double a){ x /= a; y /= a; return(*this); }
	const bool operator==(const Vertex2D &u){ return((x == u.x) && (y == u.y)); }
	const bool operator!=(const Vertex2D &u){ return((x != u.x) || (y != u.y)); }

	Vertex2D operator +(){ return *this; }
	Vertex2D operator -(){ return Vertex2D(-x, -y); }

	double abs(){
		double result = 0;
		for (int i = 0; i<2; i++)
			result += X[i] * X[i];
		return sqrt(result);
	}

	friend Vertex2D operator *(const Vertex2D &u, const double a){ Vertex2D result(u); result *= a; return result; }
	friend Vertex2D operator *(const double a, const Vertex2D &u){ Vertex2D result(u); result *= a; return result; }
	friend double operator *(const Vertex2D &u, const Vertex2D &v){ double result; result = u.x*v.x + u.y*v.y; return result; }
	friend Vertex2D operator +(const Vertex2D &u, const Vertex2D &v){ Vertex2D result(u); result += v; return result; }
	friend Vertex2D operator -(const Vertex2D &u, const Vertex2D &v){ Vertex2D result(u); result -= v; return result; }
	friend Vertex2D operator /(const Vertex2D &u, const double a){ Vertex2D result(u); result /= a; return result; }
	friend std::ostream& operator<<(std::ostream& stream, const Vertex2D& v){ return stream << '[' << v.x << ", " << v.y << ']'; }
};

class Vertex2Df{
public:
	union { struct { float x, y; }; float X[2]; };

	Vertex2Df() :x(0), y(0){}
	Vertex2Df(float _x, float _y) :x(_x), y(_y){}
	Vertex2Df(float _X[2]) :x(_X[0]), y(_X[1]){}

	Vertex2Df &operator=(const float &v){ this->x = v; this->y = v; return(*this); }
	Vertex2Df &operator=(const Vertex2Df &v){ this->x = v.x; this->y = v.y; return(*this); }
	Vertex2Df &operator+=(const Vertex2Df &u){ x += u.x; y += u.y; return(*this); }
	Vertex2Df &operator-=(const Vertex2Df &u){ x -= u.x; y -= u.y; return(*this); }
	Vertex2Df &operator*=(const Vertex2Df &u){ x *= u.x; y *= u.y; return(*this); }
	Vertex2Df &operator*=(const float a){ x *= a; y *= a; return(*this); }
	Vertex2Df &operator/=(const float a){ x /= a; y /= a; return(*this); }
	const bool operator==(const Vertex2Df &u){ return((x == u.x) && (y == u.y)); }
	const bool operator!=(const Vertex2Df &u){ return((x != u.x) || (y != u.y)); }

	float abs(){
		float result = 0;
		for (int i = 0; i<2; i++)
			result += X[i] * X[i];
		return sqrt(result);
	}

	Vertex2Df operator +(){ return *this; }
	Vertex2Df operator -(){ return Vertex2Df(-x, -y); }
	friend Vertex2Df operator *(const Vertex2Df &u, const float a){ Vertex2Df result(u); result *= a; return result; }
	friend Vertex2Df operator *(const float a, const Vertex2Df &u){ Vertex2Df result(u); result *= a; return result; }
	friend float operator *(const Vertex2Df &u, const Vertex2Df &v){ float result; result = u.x*v.x + u.y*v.y; return result; }
	friend Vertex2Df operator +(const Vertex2Df &u, const Vertex2Df &v){ Vertex2Df result(u); result += v; return result; }
	friend Vertex2Df operator -(const Vertex2Df &u, const Vertex2Df &v){ Vertex2Df result(u); result -= v; return result; }
	friend Vertex2Df operator /(const Vertex2Df &u, const float a){ Vertex2Df result(u); result /= a; return result; }
	friend std::ostream& operator<<(std::ostream& stream, const Vertex2Df& v){ return stream << '[' << v.x << ", " << v.y << ']'; }
};

//****************************************************************************
// 3D double Vertex Class
//****************************************************************************
class Vertex3D{
public:
	union { struct { double x, y, z; }; double X[3]; };
	Vertex3D() :x(0), y(0), z(0){}
	Vertex3D(double _x, double _y, double _z) :x(_x), y(_y), z(_z){}
	Vertex3D(double _X[3]) :x(_X[0]), y(_X[1]), z(_X[2]){}

	Vertex3D &operator=(const double &v){ x = v; y = v; z = v; return(*this); }
	Vertex3D &operator=(const Vertex3D &v){ x = v.x; y = v.y; z = v.z; return(*this); }
	Vertex3D &operator+=(const Vertex3D &u){ x += u.x; y += u.y; z += u.z; return(*this); }
	Vertex3D &operator-=(const Vertex3D &u){ x -= u.x; y -= u.y; z -= u.z; return(*this); }
	Vertex3D &operator*=(const Vertex3D &u){ x *= u.x; y *= u.y; z *= u.z; return(*this); }
	Vertex3D &operator*=(const double a){ x *= a; y *= a; z *= a; return(*this); }
	Vertex3D &operator/=(const double a){ x /= a; y /= a; z /= a; return(*this); }
	const bool operator==(const Vertex3D &u){ return((x == u.x) && (y == u.y) && (z == u.z)); }
	const bool operator!=(const Vertex3D &u){ return((x != u.x) || (y != u.y) || (z != u.z)); }
	const bool operator!=(const double &u){ return((x != u) || (y != u) || (z != u)); }
	const bool operator<(const Vertex3D &u){ return((x<u.x) && (y<u.y) && (z<u.z)); }
	const bool operator>(const Vertex3D &u){ return((x>u.x) && (y>u.y) && (z>u.z)); }
	const bool operator<(const double &u){ return(x<u) && (y<u) && (z<u); }
	const bool operator>(const double &u){ return(x>u) && (y>u) && (z>u); }
	const bool operator<=(const double &u){ return(x <= u) && (y <= u) && (z <= u); }
	const bool operator>=(const double &u){ return(x >= u) && (y >= u) && (z >= u); }

	Vertex3D operator +(){ return *this; }
	Vertex3D operator -(){ return Vertex3D(-x, -y, -z); }

	double abs(){
		double result = 0;
		for (int i = 0; i<3; i++)
			result += X[i] * X[i];
		return sqrt(result);
	}
	Vertex3D cross(Vertex3D &v){
		Vertex3D result((y*v.z - z*v.y), (z*v.x - x*v.z), (x*v.y - y*v.x));
		return result;
	}

	friend Vertex3D operator *(const Vertex3D &u, const double a){ Vertex3D result(u); result *= a; return result; }
	friend Vertex3D operator *(const double a, const Vertex3D &u){ Vertex3D result(u); result *= a; return result; }
	friend double operator *(const Vertex3D &u, const Vertex3D &v){ double result; result = u.x*v.x + u.y*v.y + u.z*v.z; return result; }
	friend Vertex3D operator +(const Vertex3D &u, const Vertex3D &v){ Vertex3D result(u); result += v; return result; }
	friend Vertex3D operator -(const Vertex3D &u, const Vertex3D &v){ Vertex3D result(u); result -= v; return result; }
	friend Vertex3D operator /(const Vertex3D &u, const double a){ Vertex3D result(u); result /= a; return result; }
	friend Vertex3D operator %(const Vertex3D &u, const Vertex3D &v){ return Vertex3D(u.y*v.z - u.z*v.y, u.z*v.x - u.x*v.z, u.x*v.y - u.y*v.x); }
	friend std::ostream& operator<<(std::ostream& stream, const Vertex3D& v){ return stream << '(' << v.x << "," << v.y << "," << v.z << ')'; }
};

class Vertex3Df{
public:
	union { struct { float x, y, z; }; float X[3]; };
	Vertex3Df() :x(0), y(0), z(0){}
	Vertex3Df(float _x, float _y, float _z) :x(_x), y(_y), z(_z){}
	Vertex3Df(float _X[3]) :x(_X[0]), y(_X[1]), z(_X[2]){}

	Vertex3Df &operator=(const float &v){ x = v; y = v; z = v; return(*this); }
	Vertex3Df &operator=(const Vertex3Df &v){ x = v.x; y = v.y; z = v.z; return(*this); }
	Vertex3Df &operator+=(const Vertex3Df &u){ x += u.x; y += u.y; z += u.z; return(*this); }
	Vertex3Df &operator-=(const Vertex3Df &u){ x -= u.x; y -= u.y; z -= u.z; return(*this); }
	Vertex3Df &operator*=(const Vertex3Df &u){ x *= u.x; y *= u.y; z *= u.z; return(*this); }
	Vertex3Df &operator*=(const float a){ x *= a; y *= a; z *= a; return(*this); }
	Vertex3Df &operator/=(const float a){ x /= a; y /= a; z /= a; return(*this); }
	const bool operator==(const Vertex3Df &u){ return((x == u.x) && (y == u.y) && (z == u.z)); }
	const bool operator!=(const Vertex3Df &u){ return((x != u.x) || (y != u.y) || (z != u.z)); }
	const bool operator!=(const float &u){ return((x != u) || (y != u) || (z != u)); }

	Vertex3Df operator +(){ return *this; }
	Vertex3Df operator -(){ return Vertex3Df(-x, -y, -z); }

	float abs(){
		float result = 0;
		for (int i = 0; i<3; i++)
			result += X[i] * X[i];
		return sqrt(result);
	}
	Vertex3Df cross(Vertex3Df &v){
		Vertex3Df result((y*v.z - z*v.y), (z*v.x - x*v.z), (x*v.y - y*v.x));
		return result;
	}

	friend Vertex3Df operator *(const Vertex3Df &u, const float a){ Vertex3Df result(u); result *= a; return result; }
	friend Vertex3Df operator *(const float a, const Vertex3Df &u){ Vertex3Df result(u); result *= a; return result; }
	friend float operator *(const Vertex3Df &u, const Vertex3Df &v){ float result; result = u.x*v.x + u.y*v.y + u.z*v.z; return result; }
	friend Vertex3Df operator +(const Vertex3Df &u, const Vertex3Df &v){ Vertex3Df result(u); result += v; return result; }
	friend Vertex3Df operator -(const Vertex3Df &u, const Vertex3Df &v){ Vertex3Df result(u); result -= v; return result; }
	friend Vertex3Df operator /(const Vertex3Df &u, const float a){ Vertex3Df result(u); result /= a; return result; }
	friend Vertex3Df operator %(const Vertex3Df &u, const Vertex3Df &v){ return Vertex3Df(u.y*v.z - u.z*v.y, u.z*v.x - u.x*v.z, u.x*v.y - u.y*v.x); }
	friend std::ostream& operator<<(std::ostream& stream, const Vertex3Df& v){ return stream << '(' << v.x << "," << v.y << "," << v.z << ')'; }
};

//****************************************************************************
// N*M dobule Matrix Class
//****************************************************************************
class Matrix{
public:
	int row;
	int col;
	double *X;

	Matrix() :row(0), col(0), X(0){}
	Matrix(int _n, int _m) :row(_n), col(_m){ X = new double[row*col]; for (int i = 0; i<row*col; i++)X[i] = 0; }
	Matrix(const Matrix &u){ row = u.row; col = u.col; X = new double[row*col]; for (int i = 0; i<row*col; i++)X[i] = u.X[i]; }
	~Matrix(){ if (row*col > 0) delete[]X; row = col = 0; }

private:
	// errorメッセージ
	void Error(const Matrix &u){
		std::cout << "大きさが違います" << std::endl;
		std::cout << "row1 = " << row << ", col1 = " << col << std::endl;
		std::cout << "row2 = " << u.row << ", col2 = " << u.col << std::endl;
	}

public:
	// Matと違って =演算子はコピー
	Matrix &operator=(const Matrix &u){
		if (&u == this)return *this;
		if (row == u.row && col == u.col){
			for (int i = 0; i<row*col; i++) X[i] = u.X[i];
			return *this;
		}
		else {
			if (row*col > 0) delete[]X; row = u.row; col = u.col; X = new double[row*col];
			for (int i = 0; i<row*col; i++) X[i] = u.X[i];
		}
		return *this;
	}
	Matrix &operator+=(const Matrix &u){
		if (row == u.row&&col == u.col){
			for (int i = 0; i < row*col; i++)
				X[i] += u.X[i];
			return(*this);
		}
		else Error(u);
	}
	Matrix &operator-=(const Matrix &u)
	{
		if (row == u.row&&col == u.col){
			for (int i = 0; i<row*col; i++)
				X[i] -= u.X[i];
			return(*this);
		}
		else Error(u);
	}
	Matrix &operator*=(const double a){
		for (int i = 0; i<row*col; i++)
			X[i] *= a; return(*this);
	}
	Matrix &operator/=(const double a){
		for (int i = 0; i<row*col; i++)
			X[i] /= a; return(*this);
	}
	const bool operator==(const Matrix &u){
		if (col != u.col || row != u.row) return false;
		for (int i = 0; i<row*col; i++){
			if (X[i] != u.X[i]) return false;
		}
		return true;
	}
	const bool operator!=(const Matrix &u){
		if (col != u.col || row != u.row) return true;
		for (int i = 0; i<row*col; i++){
			if (X[i] != u.X[i]) return true;
		}
		return false;
	}

	Matrix &operator +(){ return(*this); }
	Matrix &operator -(){ for (int i = 0; i<row*col; i++)X[i] = -X[i]; return(*this); }

	friend Matrix operator *(const Matrix &u, const double a){ Matrix result(u); result *= a; return result; }
	friend Matrix operator *(const double a, const Matrix &u){ Matrix result(u); result *= a; return result; }
	friend Matrix operator *(const Matrix &u, const Matrix &v){
		if (u.col != v.row){
			std::cout << "掛け算失敗" << std::endl;
			std::cout << u.row << "x" << u.col << " " << v.row << "x" << v.col << std::endl;
		}
		Matrix result(u.row, v.col);
		for (int i = 0; i<result.row; i++)
		for (int j = 0; j<result.col; j++)
		for (int k = 0; k < u.col; k++){
			result.X[result.col*i + j] += (u.X[u.col*i + k] * v.X[v.col*k + j]);
		}
		return result;
	}

	// 座標変換行列と三次元点をつっこむと勝手に計算する
	friend Matrix operator *(const Matrix &u, const Vertex3D &v){
		if (u.row != 4 || u.row != u.col){
			std::cout << "座標変換できないっす" << std::endl;
			std::cout << u.row << "x" << u.col << std::endl;
			return Matrix::eye(4, 1);
		}
		Matrix AX(4, 1);
		AX.X[0] = v.x; AX.X[1] = v.y; AX.X[2] = v.z; AX.X[3] = 1.0;
		return  u * AX;
	}
	friend Matrix operator *(const Matrix &u, const Vertex3Df &v){
		if (u.row != 4 || u.row != u.col){
			std::cout << "座標変換できないっす" << std::endl;
			std::cout << u.row << "x" << u.col << std::endl;
			return Matrix::eye(4, 4);
		}
		Matrix AX(4, 1);
		AX.X[0] = (double)v.x; AX.X[1] = (double)v.y; AX.X[2] = (double)v.z; AX.X[3] = 1.0;
		return  u * AX;
	}
	friend Matrix operator +(const Matrix &u, const Matrix &v){ Matrix result(u); result += v; return result; }
	friend Matrix operator -(const Matrix &u, const Matrix &v){ Matrix result(u); result -= v; return result; }
	friend Matrix operator /(const Matrix &u, const double a){ Matrix result = u; result /= a; return result; }

	// std::cout
	friend std::ostream& operator<<(std::ostream& stream, const Matrix& v){
		std::string result = "[";
		for (int i = 0; i < v.row; i++){
			for (int j = 0; j < v.col; j++){
				result += std::to_string(v.X[v.col*i + j]);
				result += " ";
			}
			result += "\n";
		}
		result.pop_back();
		result += "]";

		return stream << result;

	}

	// 単位行列
	static Matrix eye(const int _n, const int _m){
		Matrix result(_n, _m);
		for (int i = 0; i < _n&&i < _m; i++){
			result.X[i*result.col + i] = 1;
		}
		return result;
	}
	// 回転行列
	static Matrix rotX(const double _deg){
		Matrix result = Matrix::eye(4, 4);
		double rad = _deg * M_PI / 180.0;
		result.X[5] = cos(rad); result.X[6] = -sin(rad);
		result.X[9] = sin(rad); result.X[10] = cos(rad);
		return result;
	}
	static Matrix rotY(const double _deg){
		Matrix result = Matrix::eye(4, 4);
		double rad = _deg * M_PI / 180.0;
		result.X[0] = cos(rad); result.X[2] = sin(rad);
		result.X[8] = -sin(rad); result.X[10] = cos(rad);
		return result;
	}
	static Matrix rotZ(const double _deg){
		Matrix result = Matrix::eye(4, 4);
		double rad = _deg * M_PI / 180.0;
		result.X[0] = cos(rad); result.X[1] = -sin(rad);
		result.X[4] = sin(rad); result.X[5] = cos(rad);
		return result;
	}

	double det(void);	// 行列式
	Matrix trn();		// 転置	
	Matrix inv();		// 逆行列

	void free();
	void malloc(int _n, int _m);
	void uni();
};


//****************************************************************************
// Vector and Matrix Calculation Function
//****************************************************************************
namespace MathCalMethod{
	double lu(int n, double *a, int *ip);
	double Inverse_LU(double *a_inv, double *a, int n);
	double Inverse_Gauss(Matrix *inv, Matrix *a);
	double nABS(double *_src, int _len);
};