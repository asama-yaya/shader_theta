#version 330
// shadertype=<glsl>
// vert

// 渡す変数
out vec3 texcoord;

// 受け取る行列
uniform mat4 matM;
uniform float angle; 

// おまじないに近いもの
layout (location = 0) in vec4 pv; 

void main(void){
	float sita = angle / 180.0 * 3.1415;

	texcoord.x = pv.x * tan(sita);
	texcoord.y = pv.y * tan(sita) * (360.0/640.0);
	texcoord.z = -1.0;

	vec4 aaa = vec4(texcoord, 1.0);
	vec4 bbb = matM * aaa;
	texcoord = bbb.xyz;

	gl_Position = pv;
}