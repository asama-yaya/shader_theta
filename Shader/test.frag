#version 330
// shadertype=<glsl>
// frag

// テクスチャ
uniform sampler2DRect image;

// もらう変数
in vec3 texcoord;

// フレームバッファに出力するデータ
layout (location = 0) out vec4 fc1; 

// theta
const vec2 SI1 = vec2(320,360); 
const vec2 SI2 = vec2(960,360); 
vec2 Sift = vec2(0,40);
float RR = 288;
float sitamax = 90.0f/180.0f*3.1415f;

void main(void){
	//fc1 = texture(image, gl_FragCoord.xy);

	/* 立体射影 */
	// このフラグメントを通る視線の方向ベクトル
	vec3 direction = normalize(texcoord);
 
	float sita = acos(-direction.z);

	// テクスチャ座標
	vec2 st1, st2;
#if 0
	// 立体射影
	st1 = normalize(texcoord.xy) * tan(sita/2.0) / tan(sitamax/2.0) * RR + SI1 + Sift;
	st2 = normalize(texcoord.xy) * tan((sitamax*2.0 - sita)/2.0) / tan(sitamax/2.0) * RR;
#else
	// 等立体角射影 theta
	st1 = normalize(texcoord.xy) * sin(sita/2.0) / sin(sitamax/2.0) * RR + SI1 + Sift;
	st2 = normalize(texcoord.xy) * sin((sitamax*2.0 - sita)/2.0) / sin(sitamax/2.0) * RR;
#endif
	st2.x = st2.x * -1.0;
	st2 = st2 + SI2 + Sift;
  
	// 全周魚眼画像から視線方向の色をサンプリングする(角度に応じてブレンディング)		
	float w = clamp((1.1 - abs(sita)/sitamax) * 5.0, 0.0, 1.0);
	fc1 = w * texture(image, st1) + (1.0 - w) * texture(image, st2);
}


/* 等距離射影 */
//// このフラグメントを通る視線の方向ベクトル
//vec3 direction = normalize(texcoord);
  
//// テクスチャ座標
//float sita = acos(-direction.z);
//vec2 st1 = normalize(texcoord.xy) * sita / sitamax * RR + SI1 + Sift;
//vec2 st2 = normalize(texcoord.xy) * (sitamax*2.0 - sita) / sitamax * RR;
//st2.x = st2.x * -1.0;
//st2 = st2 + SI2 + Sift;
  
//// 全周魚眼画像から視線方向の色をサンプリングする
//fc = texture(image, st1);
//if(abs(sita)>sitamax){
//	fc = texture(image, st2);
//}